#![no_std]
#![no_main]

use card10_l0dable::{
    display_adv, main, println, BHI160Error as Error, Buttons, Color, Display, LineStyle, MAX86150,
};

/// Allows you to `use alloc::*;`
extern crate alloc;

main!(main);
fn main() {
    let heap_size = card10_alloc::init(128 * 1024);
    println!("Heap size: {}", heap_size);

    let result = run();
    if let Err(error) = result {
        println!("error: {}", error);
    }
}

fn run() -> Result<(), Error> {
    let display = Display::open();

    let spo2 = MAX86150::start()?;

    // VecDeque would be better?
    let mut avg = [0; 10];
    let mut avg_pos = 0;
    let mut filtered_value = 0.0;
    let mut last_sample = 0.0;
    const WIDTH: usize = 160;
    const SCALE_FACTOR: f32 = 30.0;
    const OFFSET_Y: f32 = 50.0;
    let mut history = [0.0; 256];
    let mut history_pos = 0;

    for _tick in 0.. {
        match spo2.read() {
            Ok(spo2_data) => {
                if !spo2_data.is_empty() {
                    for data in &spo2_data {
                        avg[avg_pos] = data.get_red();
                        avg_pos = (avg_pos + 1) % avg.len();
                        let avg_data = avg.iter().sum::<u32>() as f32 / avg.len() as f32;
                        // DC offset removal
                        filtered_value = 0.9 * (filtered_value + avg_data - last_sample);
                        last_sample = avg_data;

                        history[history_pos] = filtered_value;
                        history_pos = (history_pos + 1) % history.len();
                    }
                    display.clear(Color::black());
                    display_adv!(
                        display,
                        Font16,
                        160,
                        -16,
                        Color::white(),
                        Color::black(),
                        "SpO2"
                    );

                    let history_max = history.iter().fold(0.0, |acc: f32, &x| {
                        let x_abs = if x < 0.0 { -x } else { x };
                        if acc < x_abs {
                            x_abs
                        } else {
                            acc
                        }
                    });

                    let scale = (if history_max > 0.0 { history_max } else { 1.0 }) / SCALE_FACTOR;

                    let mut prev =
                        ((history[history_pos % history.len()] / scale) + OFFSET_Y) as i16;
                    for i in history_pos + 1..(WIDTH + history_pos) {
                        let value = ((history[i % history.len()] / scale) + OFFSET_Y) as i16;
                        display.line(
                            (i - history_pos - 1) as i16,
                            prev,
                            (i - history_pos) as i16,
                            value,
                            Color::white(),
                            LineStyle::Full,
                            1,
                        );
                        prev = value;
                    }
                    display.update();
                }
            }
            Err(error) => println!("error: {}", error),
        }
        let buttons = Buttons::read();
        if buttons.left_top() {
            break;
        }
    }
    Ok(())
}
