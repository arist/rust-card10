{ pkgs ? import <nixpkgs> {},
  cFirmware ? <c-firmware>,
  rkanoid ? <rkanoid>,
}:
with pkgs;

let
  l0dables = buildEnv {
    name = "l0dables";
    paths = [ rkanoid ];
    pathsToLink = [ "/apps" ];
  };
  release = import ../release.nix {
    inherit cFirmware;
    rustL0dables = l0dables;
  };
  releaseZip = stdenv.mkDerivation {
    name = "card10-combined.zip";
    nativeBuildInputs = [ release zip ];
    phases = [ "installPhase" ];
    installPhase = ''
      mkdir -p $out/nix-support

      cd ${release}/
      zip -9r $out/firmware.zip .
      echo file binary-dist $out/firmware.zip >> $out/nix-support/hydra-build-products
    '';
  };
in {
  release = lib.hydraJob release;
  release-zip = lib.hydraJob releaseZip;
}
